﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Cards;

namespace UnitTestProject
{
	[TestClass]
	public class UnitTest1
	{
		[TestMethod]
		public void Test1()
		{
			CardCollection cardCollection = new CardCollection();
			cardCollection.Add(new Card
			{
				Rank = Ranks.Ace,
				Suit = Suits.Spade,
			});
			cardCollection.Add(new Card
			{
				Rank = Ranks.Ace,
				Suit = Suits.Diamond,
			});
			cardCollection.Add(new Card
			{
				Rank = Ranks.Three,
				Suit = Suits.Spade,
			});
			cardCollection.Add(new Card
			{
				Rank = Ranks.Four,
				Suit = Suits.Spade,
			});
			cardCollection.Add(new Card
			{
				Rank = Ranks.Five,
				Suit = Suits.Spade,
			});


			//Assert.IsFalse(pairCollection.IsRoyalFlush);
			//Assert.IsFalse(pairCollection.IsStraightFlush);
			Assert.IsFalse(cardCollection.IsFourOfAKind);
			Assert.IsFalse(cardCollection.IsFullHouse);
			Assert.IsFalse(cardCollection.IsFlush);
			//Assert.IsFalse(pairCollection.IsStraight);
			Assert.IsFalse(cardCollection.IsThreeOfAKind);
			Assert.IsFalse(cardCollection.IsTwoPair);
			Assert.IsTrue(cardCollection.IsPair);

			Assert.IsTrue(cardCollection.PairCount == 1);
			Assert.IsTrue(cardCollection.ThreeOfAKindCount == 0);
			Assert.IsTrue(cardCollection.FourOfAKindCount == 0);
		}

		[TestMethod]
		public void Test2()
		{
			CardCollection cardCollection = new CardCollection();
			cardCollection.Add(new Card
			{
				Rank = Ranks.Ace,
				Suit = Suits.Spade,
			});
			cardCollection.Add(new Card
			{
				Rank = Ranks.Two,
				Suit = Suits.Diamond,
			});
			cardCollection.Add(new Card
			{
				Rank = Ranks.Three,
				Suit = Suits.Spade,
			});
			cardCollection.Add(new Card
			{
				Rank = Ranks.Four,
				Suit = Suits.Spade,
			});
			cardCollection.Add(new Card
			{
				Rank = Ranks.Five,
				Suit = Suits.Spade,
			});
			
			
			//Assert.IsFalse(pairCollection.IsRoyalFlush);
			//Assert.IsFalse(pairCollection.IsStraightFlush);
			Assert.IsFalse(cardCollection.IsFourOfAKind);
			Assert.IsFalse(cardCollection.IsFullHouse);
			Assert.IsFalse(cardCollection.IsFlush);
			Assert.IsTrue(cardCollection.IsStraight);
			Assert.IsFalse(cardCollection.IsThreeOfAKind);
			Assert.IsFalse(cardCollection.IsTwoPair);
			Assert.IsFalse(cardCollection.IsPair);

			Assert.IsTrue(cardCollection.PairCount == 1);
			Assert.IsTrue(cardCollection.ThreeOfAKindCount == 0);
			Assert.IsTrue(cardCollection.FourOfAKindCount == 0);
		}
	}

}
