﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cards
{
	public class CardPile : ICardPile
	{
		public enum PileType
		{
			Waste,
			Foundation,
			Tableau,
		}

		public string Label { get; set; }

		public PileType Type { get; set; }
		public ICollection<ICard> Cards { get; set; } = new List<ICard>();

		public override string ToString()
		{
			var builder = new StringBuilder();

			builder.Append($"{Label}:");
			foreach (var card in Cards)
				builder.Append($" {card.ShortIdentity}");

			return builder.ToString();
		}
	}

	class Deck : IDeck
	{
		public ISuitCollection Suits { get; set; }
		public uint NumberOfDecks { get; set; }

		public IRankCollection Ranks { get; set; }

		public Deck(ISuitCollection suitCollection, IRankCollection rankCollection, uint numberOfDecks)
		{
			Suits = suitCollection;
			Ranks = rankCollection;
			NumberOfDecks = numberOfDecks;
		}
	}

	/// <summary>
	/// Defines a game of Klondike at the moment but the plan is to use XML to dynamically build these.  This will
	/// potentially become an abstraction for all solitare type games
	/// </summary>
	class GameRules : IGameRules
	{
		public IEnumerable<ICardPile> WastePiles => Piles.Where(x => x.Type == CardPile.PileType.Waste);
		public IEnumerable<ICardPile> FoundationPiles => Piles.Where(x => x.Type == CardPile.PileType.Foundation);
		public IEnumerable<ICardPile> TableauPiles => Piles.Where(x => x.Type == CardPile.PileType.Tableau);

		public IDeckTableau DeckPile { get; set; }

		public Action NewGameAction;
		public Action InitialDealAction;

		public GameRules()
		{
			//	DeckPile and Deck can probably be merged into one class
			Deck = new Deck(new SuitCollection(), new RankCollection(), 1);
			Piles = new List<ICardPile>
			{
				new CardPile { Type = CardPile.PileType.Waste, Label = "Waste" },
				new CardPile { Type = CardPile.PileType.Foundation, Label = "Foundation0" },
				new CardPile { Type = CardPile.PileType.Foundation, Label = "Foundation1" },
				new CardPile { Type = CardPile.PileType.Foundation, Label = "Foundation2" },
				new CardPile { Type = CardPile.PileType.Foundation, Label = "Foundation3" },
				new CardPile { Type = CardPile.PileType.Tableau, Label = "Tableau0" },
				new CardPile { Type = CardPile.PileType.Tableau, Label = "Tableau1" },
				new CardPile { Type = CardPile.PileType.Tableau, Label = "Tableau2" },
				new CardPile { Type = CardPile.PileType.Tableau, Label = "Tableau3" },
				new CardPile { Type = CardPile.PileType.Tableau, Label = "Tableau4" },
				new CardPile { Type = CardPile.PileType.Tableau, Label = "Tableau5" },
				new CardPile { Type = CardPile.PileType.Tableau, Label = "Tableau6" },
			};

			DeckPile = new DeckPile<SuitCollection, RankCollection>();

			NewGameAction = () =>
			{
				//	Remove any existing cards
				foreach (var pile in Piles)
				{
					pile.Cards.Clear();
					DeckPile.Cards.Clear();
				}

				//	Reinitialise the Deck
				DeckPile.AddPack();

				//	Shuffle the cards
				DeckPile.Shuffle();
			};

			InitialDealAction = () => 
			{
				int t = 0;
				foreach (var tableau in TableauPiles)
				{
					DeckPile.DealTo(tableau, new string('D', t) +  "U");
					++t;
				}
			};
		}

		public void NewGame()
		{
			Console.WriteLine("Starting new game");
			NewGameAction.Invoke();
		}

		public void StartGame()
		{
			Console.WriteLine("Performing initial deal");
			InitialDealAction.Invoke();
		}

		public IDeck Deck { get; set; }
		public ICollection<ICardPile> Piles { get; set; }
		public IInitialDealRule InitialDealRule { get; set; }

		public override string ToString()
		{
			var builder = new StringBuilder();

			builder.AppendLine("Waste piles");
			foreach (var wastePile in WastePiles)
				builder.AppendLine($"\t{wastePile}");
			builder.AppendLine();

			builder.AppendLine("Tableau piles");
			foreach (var tableauPile in TableauPiles)
				builder.AppendLine($"\t{tableauPile}");
			builder.AppendLine();

			return builder.ToString();
		}
	}
}
