﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cards
{
	public class Suit : ISuit
	{
		public string ShortName { get; set; }

		public string FullName { get; set; }

		public uint Value { get; set; } = 0;

		public IColour Colour { get; set; }

		public string ToString(string format, IFormatProvider formatProvider) => FullName;
	}

	public interface ISuitCollection : IReadOnlyCollection<ISuit>, IEnumerable<ISuit>
	{
		ICollection<ISuit> Values { get; }
	}

	public class SuitCollection : ISuitCollection
	{
		public static readonly ISuit Spade = new Suit { FullName = "Spade", ShortName = "♠", Colour = Colours.Black };
		public static readonly ISuit Heart = new Suit { FullName = "Heart", ShortName = "♥", Colour = Colours.Red };
		public static readonly ISuit Club = new Suit { FullName = "Club", ShortName = "♣", Colour = Colours.Black };
		public static readonly ISuit Diamond = new Suit { FullName = "Diamond", ShortName = "♦", Colour = Colours.Red };

		public ICollection<ISuit> Values => new List<ISuit>
		{
			Spade, Heart, Club, Diamond
		};

		public int Count => Values.Count;

		public IEnumerator<ISuit> GetEnumerator()
		{
			foreach (var value in Values)
			{
				yield return value;
			}
		}

		IEnumerator IEnumerable.GetEnumerator()
		{
			throw new NotImplementedException();
		}
	}

	public class Colour : IColour
	{
		public string ShortName { get; set; }
		public string FullName { get; set; }

		public string ToString(string format, IFormatProvider formatProvider) => FullName;
	}

	public class Colours
	{
		public static readonly IColour Red = new Colour { FullName = "Red", ShortName = "R" };
		public static readonly IColour Black = new Colour { FullName = "Black", ShortName = "B" };
	}
}