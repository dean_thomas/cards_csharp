﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cards
{
	public class Card : ICard
	{
		public IRank Rank { get; set; }
		public ISuit Suit { get; set; }

		public string ShortName => $"{Rank.ShortName}{Suit.ShortName}";
		public string FullName => $"{Rank.FullName} of {Suit.FullName}s";

		public bool ShowFace { get; set; }

		public string ShortIdentity => ShowFace ? ShortName : "##";
		public string FullIdentity => ShowFace ? FullName : "<hidden>";

		public string ToString(string format, IFormatProvider formatProvider) => FullName;
	}
}
