﻿using System;
using System.Collections.Generic;

namespace Cards
{
	public interface ICard : IFormattable
	{
		/// <summary>
		/// Represents the suit of the Card
		/// </summary>
		ISuit Suit { get;}
		/// <summary>
		/// Represents the rank of the Card
		/// </summary>
		IRank Rank { get;}
		/// <summary>
		/// Represents the name of the card in a symbolic form (i.e. "5♠" for "Five of Spades") 
		/// </summary>
		string ShortName { get; }
		/// <summary>
		/// Returns either the ShortName of the card if it is face up 
		/// or "##" if the card is face down
		/// </summary>
		string ShortIdentity { get; }
		/// <summary>
		/// Represents the name of the card in full form (i.e. "Five of Spades")
		/// </summary>
		string FullName { get; }
		/// <summary>
		/// Returns either the ShortName of the card if it is face up 
		/// or "<hidden>" if the card is face down
		/// </summary>
		string FullIdentity { get; }
		/// <summary>
		/// Determines if the face of the card is shown
		/// </summary>
		bool ShowFace { get; set; }
		
	}

	public interface ISuit : IFormattable
	{
		string ShortName { get;}
		string FullName { get; }

		uint Value { get; }
		IColour Colour { get; }
	}

	public interface IColour : IFormattable
	{
		string ShortName { get; }
		string FullName { get; }
	}

	public interface IRank : IFormattable
	{
		string ShortName { get; }
		string FullName { get; }

		int Value { get; }
	}

	public interface IHand
	{
		ICollection<ICard> Cards { get; }
		bool IsStraight { get; }
		bool IsFlush { get; }
		int PairCount { get; }
		int ThreeOfAKindCount { get; }
		int FourOfAKindCount { get; }
	}

	public interface IPokerHand : IHand
	{
		bool IsRoyalFlush { get; }
		bool IsStraightFlush { get; }
		bool IsFourOfAKind { get; }
		bool IsFullHouse { get; }
		bool IsThreeOfAKind { get; }
		bool IsTwoPair { get; }
		bool IsPair { get; }
	}

	public interface IDeckTableau
	{
		void Deal();
		Action DealAction { get; set; }

		Queue<ICard> Cards { get; }

		void AddPack();

		int DealTo(ICardPile pile, string pattern);

		void Shuffle();
	}
}