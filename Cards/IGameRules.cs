﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static Cards.CardPile;

namespace Cards
{
	public interface ICardPile
	{
		PileType Type { get; }
		string Label { get; }
		ICollection<ICard> Cards { get; }
	}

	interface IDeck
	{
		ISuitCollection Suits { get; set; }
		IRankCollection Ranks { get; set; }

		uint NumberOfDecks { get; set; }
	}

	interface IInitialDealRule
	{
		Action DealAction { get; set; }
	}

	interface IGameRules
	{
		IDeck Deck { get; }

		IInitialDealRule InitialDealRule { get; set; }
	}
}
