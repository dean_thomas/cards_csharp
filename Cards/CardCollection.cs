﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cards
{
	public class CardCollection<S, R> : IPokerHand
		where S : ISuitCollection, new()
		where R : IRankCollection, new()
	{
		private S suits = new S();
		private R ranks = new R();

		Dictionary<ISuit, uint> suitCounts = new Dictionary<ISuit, uint>();
		Dictionary<IRank, uint> rankCounts = new Dictionary<IRank, uint>();

		private void Evaluate()
		{
			suitCounts.Clear();
			rankCounts.Clear();

			foreach (var card in Cards)
			{
				//	Evaluate suits
				if (suitCounts.ContainsKey(card.Suit))
					suitCounts[card.Suit]++;
				else
					suitCounts.Add(card.Suit, 1);

				//	Evaluate ranks
				if (rankCounts.ContainsKey(card.Rank))
					rankCounts[card.Rank]++;
				else
					rankCounts.Add(card.Rank, 1);
			}

			foreach (var suit in suitCounts)
			{
				Debug.WriteLine($"{suit.Key}: {suit.Value}");
			}

			foreach (var rank in rankCounts)
			{
				Debug.WriteLine($"{rank.Key}: {rank.Value}");
			}

		}

		/// <summary>
		/// Creates a list of integer values required for a high straight
		/// </summary>
		private List<int> ranksHighStraight => Enumerable.Range(ranks.Values.Max(t => t.Value) - (Cards.Count - 2), Cards.Count - 1).Concat(new List<int> { 1 }).ToList();

		/// <summary>
		/// Creates a list of sequential integers from the value of the lowest card in the hand
		/// </summary>
		private List<int> normalStraight => Enumerable.Range(rankCounts.Keys.Min(t => t.Value), Cards.Count).ToList();

		/// <summary>
		/// Helper function for mapping cards in the hand to their integer values
		/// </summary>
		private List<int> cardRanks => rankCounts.Keys.Select(t => t.Value).ToList();

		public void DebugStraight()
		{
			foreach (var x in ranksHighStraight)
			{
				Console.Write($"#{x}");
			}
			Console.WriteLine($"High straight diff count = {ranksHighStraight.Except(cardRanks).Count()}");

			foreach (var x in normalStraight)
			{
				Console.Write($"#{x}");
			}
			Console.WriteLine($"Normal straight diff count = {normalStraight.Except(cardRanks).Count()}");

		}

		/// <summary>
		/// Computes if the hand is a high straight by computing the difference in ranks with the current
		/// cards (i.e. { 10, J, Q, K, A } - { 4, 5, 6, 3, 2 }) == 10
		/// </summary>
		public bool IsHighStraight => ranksHighStraight.Except(cardRanks).Count() == 0;

		/// <summary>
		/// Computes if the hand is a normal straight by computing the difference in ranks with the current
		/// cards (i.e. { 2, 3, 4, 5, 6 } - { 4, 5, 6, 3, 2 }) == 0
		/// </summary>
		public bool IsRegularStraight => normalStraight.Except(cardRanks).Count() == 0;
		public ICollection<ICard> Cards { get; }

		public int PairCount => rankCounts.Where(x => x.Value == 2).ToList().Count;
		public int ThreeOfAKindCount => rankCounts.Where(x => x.Value == 3).ToList().Count;
		public int FourOfAKindCount => rankCounts.Where(x => x.Value == 4).ToList().Count;

		public bool IsRoyalFlush => IsHighStraight && IsFlush;
		public bool IsStraightFlush => IsStraight && IsFlush;
		public bool IsFourOfAKind => Cards.Count == 5 && FourOfAKindCount == 1;
		public bool IsFullHouse => Cards.Count == 5 && ThreeOfAKindCount == 1 && PairCount == 1;
		public bool IsFlush => suitCounts.Count == 1;
		public bool IsStraight => IsHighStraight || IsRegularStraight;
		public bool IsThreeOfAKind => Cards.Count == 5 && ThreeOfAKindCount == 1;
		public bool IsTwoPair => Cards.Count == 5 && PairCount == 2;
		public bool IsPair => Cards.Count == 5 && PairCount == 1;
		
		public CardCollection()
		{
			Cards = new List<ICard>();

			Evaluate();
		}

		public void Add(ICard card)
		{
			Cards.Add(card);

			Evaluate();
		}

		public override string ToString()
		{
			StringBuilder builder = new StringBuilder();

			bool first = true;
			foreach (var card in Cards)
			{
				if (!first) builder.Append(" ");
				builder.Append(card.ShortName);
				first = false;
			}

			builder.Append(" : {");
			builder.Append($" IsRoyalFlush: {IsRoyalFlush}");
			builder.Append($" IsStraightFlush: {IsStraightFlush}");
			builder.Append($", IsFourOfAKind: {IsFourOfAKind}");
			builder.Append($", IsFullHouse: {IsFullHouse}");
			builder.Append($", IsFlush: {IsFlush}");
			builder.Append($", IsStraight: {IsStraight}");
			builder.Append($", IsThreeOfAkind: {IsThreeOfAKind}");
			builder.Append($", IsTwoPair: {IsTwoPair}");
			builder.Append($", IsPair: {IsPair}");
			builder.Append(" }");

			return builder.ToString();
		}
	}
}
