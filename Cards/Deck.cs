﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cards
{
	public class DeckPile<S, R> : IDeckTableau
		where S : ISuitCollection, new()
		where R : IRankCollection, new()
	{
		private S suits = new S();
		private R ranks = new R();

		Random random;

		//	todo: check the most efficient collection for shuffling the data
		public Queue<ICard> Cards { get; set; } = new Queue<ICard>();

		public DeckPile()
		{
			random = new Random();
		}

		public DeckPile(int seed)
		{
			random = new Random(seed);
		}

		public Action DealAction
		{
			get
			{
				throw new NotImplementedException();
			}

			set
			{
				throw new NotImplementedException();
			}
		}

		public void Deal()
		{
			if (DealAction != null)
				DealAction.Invoke();
		}

		public void Shuffle()
		{
			shuffle(10000);
		}

		public void AddPack()
		{
			foreach (var suit in suits)
			{
				foreach (var rank in ranks)
				{
					Cards.Enqueue(new Card
					{
						Suit = suit,
						Rank = rank,
					});
				}
			}
		}

		private void shuffle(int count)
		{
			var cards = Cards.ToArray();

			for (var s = 0; s < count; ++s)
			{
				var i0 = random.Next(Cards.Count);
				var i1 = random.Next(Cards.Count);

				if (i0 != i1)
				{
					var temp = cards[i0];
					cards[i0] = cards[i1];
					cards[i1] = temp;
				}
			}

			Cards = new Queue<ICard>(cards);
		}

		public override string ToString()
		{
			StringBuilder builder = new StringBuilder();

			var first = true;
			foreach (var card in Cards)
			{
				if (!first) builder.Append(", ");
				builder.Append(card.ShortName);
				first = false;
			}

			return builder.ToString();
		}

		public int DealTo(ICardPile pile, string pattern)
		{
			int count = 0;
			foreach (char c in pattern)
			{
				if (Cards.Count > 0)
				{
					var topCard = Cards.Dequeue();
					switch (char.ToLowerInvariant(c))
					{
						case 'u':
							topCard.ShowFace = true;
							break;
						case 'd':
							topCard.ShowFace = false;
							break;
						default:
							throw new ArgumentException("Unexpected symbol in deal string.");
					}
					pile.Cards.Add(topCard);
					++count;
				}
			}

			Debug.WriteLine($"Dealt {count} cards to {pile.Label}.  Deck now contains {Cards.Count} cards.");

			return count;
		}
	}
}
