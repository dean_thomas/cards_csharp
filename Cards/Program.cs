﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Cards
{
	static class Program
	{
		/// <summary>
		/// The main entry point for the application.
		/// </summary>
		static void Main()
		{
			var rules = new GameRules();

			rules.NewGame();

			rules.StartGame();

			Console.WriteLine(rules);

			Console.ReadKey();
		}
	}
}
