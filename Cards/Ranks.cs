﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cards
{
	public class Rank : IRank
	{
		public string FullName { get; set; }

		public string ShortName { get; set; }

		public int Value { get; set; }

		public string ToString(string format, IFormatProvider formatProvider) => FullName;
	}

	public interface IRankCollection : IReadOnlyCollection<IRank>, IEnumerable<IRank>
	{
		ICollection<IRank> Values { get; }
	}

	public class RankCollection : IRankCollection
	{
		public static readonly IRank Ace = new Rank { FullName = "Ace", ShortName = "A", Value = 1 };
		public static readonly IRank Two = new Rank { FullName = "Two", ShortName = "2", Value = 2 };
		public static readonly IRank Three = new Rank { FullName = "Three", ShortName = "3", Value = 3 };
		public static readonly IRank Four = new Rank { FullName = "Four", ShortName = "4", Value = 4 };
		public static readonly IRank Five = new Rank { FullName = "Five", ShortName = "5", Value = 5 };
		public static readonly IRank Six = new Rank { FullName = "Six", ShortName = "6", Value = 6 };
		public static readonly IRank Seven = new Rank { FullName = "Seven", ShortName = "7", Value = 7 };
		public static readonly IRank Eight = new Rank { FullName = "Eight", ShortName = "8", Value = 8 };
		public static readonly IRank Nine = new Rank { FullName = "Nine", ShortName = "9", Value = 9 };
		public static readonly IRank Ten = new Rank { FullName = "Ten", ShortName = "T", Value = 10 };
		public static readonly IRank Jack = new Rank { FullName = "Jack", ShortName = "J", Value = 11 };
		public static readonly IRank Queen = new Rank { FullName = "Queen", ShortName = "Q", Value = 12 };
		public static readonly IRank King = new Rank { FullName = "King", ShortName = "K", Value = 13 };

		public ICollection<IRank> Values => new List<IRank>
		{
			Ace, Two, Three, Four, Five, Six, Seven, Eight, Nine, Ten, Jack, Queen, King
		};
		
		public int Count => Values.Count;

		public IEnumerator<IRank> GetEnumerator()
		{
			foreach (var value in Values)
				yield return value;
		}

		IEnumerator IEnumerable.GetEnumerator()
		{
			throw new NotImplementedException();
		}
	}
}
